#ifndef CONTROL_H
#define CONTROL_H

#include <QWidget>
#include "client/client.h"
//#include "gui/mainwindow.h"

#include "gui/LoginWidget/loginwidget.h"
#include "gui/chatwindow.h"
#include "gui/channelswindow.h"

#include "cache/sqlitecache.h"

#include <vector>
/**
 * @brief Controller
 * @param ip and port server
 */
class Control: public QObject
{
    Q_OBJECT
public:
    Control(Storage::SqliteConfig config);

    void run_app(int argc, char** argv);
    /**
     * @brief Start communication with server
     * @param login user's login
     * @param password user's password
     * @param command request code that would have sent on opened connection
     * it could be AutorisationRequest or RegistrationRequest
     * @todo fix noreturn
     */
    void connect_to_server(const std::string& login, const std::string& password, TypeCommand command);

    /**
     * @brief Close client socket when destroy UI controller.
     */
    ~Control() {
         qDebug() << "Destr Control" ;
        if (client) {
            client->close_connection();
        }
    }

signals:
    /**
     * @brief Show reveived message
     */
    void send_text_to_gui(const msg_text_t&);

    void send_input_code(StatusCode);

public slots:
    /**
     * @brief User autorization
     * @param login user's login
     * @param password user's password
     * @todo fix typo autorisation -> authorization
     */
    void autorisation(const std::string& login, const std::string& password) {
        std::thread th([this, login, password]() {
            try {
                connect_to_server(login, password, TypeCommand::AuthorisationRequest);
            } catch (std::exception &ex) {
                 qDebug() << "exception from thread: " << ex.what();
            }
        });
        th.detach();
    }

    /**
     * @brief User registration
     * @param login user's login
     * @param password user's password
     * @todo replace to async?
     */
    void registration(const std::string& login, const std::string& password) {
        std::thread th([this, login, password]() {
            try {
                connect_to_server(login, password, TypeCommand::RegistrationRequest);
            } catch (std::exception &ex) {
                 qDebug() << "exception from thread: " << ex.what();
            }
        });
        th.detach();
    }

    /**
     * @brief Send message
     * @param login sender's login
     * @param text message content
     * @param room_id sender's room
     */
    void get_text_from_gui(msg_text_t msg) {
        qDebug() << "send msg to server: " << msg.dt.time.hours << ":" << msg.dt.time.minutes ;
        client->send_msg_to_server(msg);
    }

    /**
     * @brief Notify UI about received message
     * @param from sender's login
     * @param text message content
     * @param dt date and time of sending the text
     */
    void text_from_client(const msg_text_t& msg) {
        send_text_to_gui(msg);
    }


    /**
     * @brief change_window
     * @todo IWindow - classes
     */
    void change_window(StatusCode a_code);
    void change_room_from_chat();

    void update_channels() {
        if (channels_window) {
            channels_window->update_list_channels();
        }
    }

    void go_channel(std::string a_channel_name) {
        if (chat_window) {
            client->set_current_channel(a_channel_name);
            chat_window->upload_history();

            channels_window->hide();
            chat_window->show();
        }
    }

private:
    Storage::SqliteConfig config;
    std::shared_ptr<Client> client;
//    std::unique_ptr<MainWindow> start_window;
    LoginWidget* login_widget;
    std::unique_ptr<ChannelsWindow> channels_window;
    std::unique_ptr<ChatWindow> chat_window;

    std::vector<uint8_t> __buffer;

    /**
     * @todo convert to 4 bytes
     */
    std::string ip = "127.0.0.1";
    int32_t port = SERVER_DEFAULT_PORT;
};

#endif // CONTROL_H
