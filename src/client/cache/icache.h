#pragma once

#include <deque>

namespace Storage {

template <class Key, class Value>
class ICache
{
public:
  virtual ~ICache() = default;

  virtual bool save(const Key& key, const Value& value) = 0;
  virtual std::deque<Value> load(const Key& key) const = 0;
};

} // namespace Storage
