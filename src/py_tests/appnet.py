#!/usr/bin/python 

import sys
import socket
import messages_pb2
import unittest
import threading

def make_socket(port=7777):
    sockobj = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sockobj.connect(('localhost', port))
    return sockobj

def connect(port=7777):
    if len(sys.argv) >= 2:
        port = int(sys.argv[1])
    return make_socket(7777)

def socket_read_n(sock, n):
    buf = b''
    while n > 0:
        data = sock.recv(n)
        if data == '':
            raise RuntimeError('unexpected connection close')
        buf += data
        n -= len(data)
    return buf

def send_message(sock, message, command_id):
    msg = message.SerializeToString()
    header = messages_pb2.Header()
    header.length = len(msg)
    header.command = command_id;
    header.version = 2;

    sock.send(header.SerializeToString())
    sock.send(message.SerializeToString())

def get_response(sock):
    len_prefix = messages_pb2.Header()
    len_buf = socket_read_n(sock, 19)
    len_prefix.ParseFromString(len_buf)
    msg_buf = socket_read_n(sock, len_prefix.length)

    msg = messages_pb2.Response()
    msg.ParseFromString(msg_buf)
   # print("response command_id = {}".format(len_prefix.command))
    return msg

def register_request(sock, name, passw):
    register_req = messages_pb2.RegRequest()
    register_req.login = name
    register_req.password = passw

    request = messages_pb2.Request()
    request.register_request.MergeFrom(register_req)

    send_message(sock, request, command_id=1)
    return get_response(sock)

def input_request(sock, name, passw):
    in_req = messages_pb2.InRequest()
    in_req.login = name
    in_req.password = passw

    request = messages_pb2.Request()
    request.input_request.MergeFrom(in_req)

    send_message(sock, request, command_id=3)
    return get_response(sock)

def join_channel_request(sock, client_id, channel_name):
    join_req = messages_pb2.JoinRoomRequest()
    join_req.client_id = client_id
    join_req.channel_name = channel_name

    request = messages_pb2.Request()
    request.join_room_request.MergeFrom(join_req)

    send_message(sock, request, command_id=7)
    return get_response(sock)

def history_request(sock, client_id, channel_name):
    story_req = messages_pb2.HistoryRequest()
    story_req.client_id = client_id
    story_req.channel_name = channel_name

    request = messages_pb2.Request()
    request.history_request.MergeFrom(story_req)

    send_message(sock, request, command_id=10)
    return get_response(sock)

def text_request(sock, login, room_id, channel_name, text):
    text_req = messages_pb2.TextRequest()
    text_req.login = login
    text_req.room_id = room_id
    text_req.channel_name = channel_name
    text_req.text = text

    request = messages_pb2.Request()
    request.text_request.MergeFrom(text_req)

    send_message(sock, request, command_id=5)
    return get_response(sock)