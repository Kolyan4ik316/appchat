#pragma once

#include "gtest/gtest.h"
// #include "gmock/gmock.h"
#include "backend/channel/channel.h"
#include "backend/backend.h"

// using ::testing::_;
// using ::testing::SetArgReferee;
// using ::testing::DoAll;
// using ::testing::Return;

// class LibBackendMock : public IBackend {
// public:
//     LibBackendMock(Storage::database_ptr a_db) {}
//     MOCK_METHOD0(configure, void(void));
//     MOCK_METHOD3(process_msg, void(connection_ptr, Serialize::Header, std::vector<uint8_t>));
//     MOCK_METHOD2(send_to_frontend, void(identifier_t, const msg_text_t&));
//     MOCK_METHOD1(leave_from_channels, void(connection_ptr));
//     MOCK_METHOD0(get_database, Storage::database_ptr(void));
// };

class TestBackend final : public IBackend {
public:
    TestBackend(Storage::database_ptr a_db) : m_db(a_db) {} 

    // void configure() override {}

    void process_msg(connection_ptr, Serialize::Header header, std::vector<uint8_t> buffer_msg) override {}

    void send_to_frontend(identifier_t a_client_id, const msg_text_t& a_msg) override {}

    void send_history_frontend(identifier_t a_client_id, const std::string& channel_name, 
                                                const std::deque<msg_text_t>& history) override {}

    void leave_from_channels(connection_ptr a_frontend_ptr) override {}

     Storage::database_ptr get_database() override {
         return m_db;
     }

      ~TestBackend() {}
private:
    Storage::database_ptr m_db;
};