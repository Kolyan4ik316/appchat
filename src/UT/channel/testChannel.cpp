#include "gtest/gtest.h"

#include "LibBackendMock.h"

#include "backend/channel/channel.h"
#include "backend/backend.h"
#include <filesystem>

namespace fs = std::filesystem;

class TestChannelConfig : public ::testing::Test {
protected:
    Storage::DatabaseConfiguration db_config;
    Storage::database_ptr m_db;  
    std::shared_ptr<IBackend> m_backend_ptr;

    static constexpr auto author = "author_msg";
    static constexpr auto channel_name = "test_name_channel";
    static constexpr auto text = "test messages";

    Config::CreateChannelConfig config;

    std::atomic<std::size_t> id_result_func = 0;
    void fake_send_to_frontend(identifier_t client_id, const msg_text_t& msg) {
        ++id_result_func;
    }
    
};

class TestChannel : public TestChannelConfig {
public:
    void SetUp() {
        db_config.FolderPath = std::string(fs::current_path()) + "/Temp/";
        db_config.ConnectionString = "file://" + db_config.FolderPath + "Test.db";
        m_db = std::make_shared<Storage::SqliteDatabase>(db_config);
        ASSERT_EQ(m_db->open(), true);

        m_db->create_table_history();
        m_db->create_table_logins();

        m_backend_ptr = std::make_shared<TestBackend>(m_db);

        config.channel_name = channel_name; 
        config.db = m_db; 

        id_result_func = 0;
    };

    void TearDown() override {
        m_db->close();
        fs::remove(db_config.FolderPath + "Test.db");
        fs::remove_all(db_config.FolderPath);
    };
};

TEST_F(TestChannel, testChannelName) {
    auto a_channel_ptr = std::make_shared<Channel>(config);
    EXPECT_EQ(a_channel_ptr->get_channel_name(), channel_name);
};

TEST_F(TestChannel, testChannelOneJoin) {
    identifier_t a_client_id = 117;
    auto a_channel_ptr = std::make_shared<Channel>(config);

    Config::JoinChannelConfig join_config;
    join_config.client_id = a_client_id;
    join_config.send_front_callback =  [this](identifier_t client_id, const msg_text_t& msg) {
        this->fake_send_to_frontend(client_id, msg);
    };

    a_channel_ptr->join_channel(join_config);

    auto a_subs = a_channel_ptr->get_subscribers();
    EXPECT_EQ(a_subs.size(), 1);
    EXPECT_EQ(a_subs.front(), a_client_id);
};

TEST_F(TestChannel, testChannelOneLeave) {
    identifier_t a_client_id = 123;
    auto a_channel_ptr = std::make_shared<Channel>(config);
    Config::JoinChannelConfig join_config;
    join_config.client_id = a_client_id;
    join_config.send_front_callback =  [this](identifier_t client_id, const msg_text_t& msg) {
        this->fake_send_to_frontend(client_id, msg);
    };

    a_channel_ptr->join_channel(join_config);
    auto a_subs = a_channel_ptr->get_subscribers();
    EXPECT_EQ(a_subs.size(), 1);
    EXPECT_EQ(a_subs.front(), a_client_id);

    a_channel_ptr->leave(a_client_id);
    a_subs = a_channel_ptr->get_subscribers();
    EXPECT_EQ(a_subs.empty(), true);
};

TEST_F(TestChannel, testChannelOneNotify) {
    identifier_t a_client_id = 127;
    auto a_channel_ptr = std::make_shared<Channel>(config);
    Config::JoinChannelConfig join_config;
    join_config.client_id = a_client_id;
    join_config.send_front_callback =  [this](identifier_t client_id, const msg_text_t& msg) {
        this->fake_send_to_frontend(client_id, msg);
    };

    a_channel_ptr->join_channel(join_config);

    auto author = "author";
    auto text = "msg text";
    DateTime dt;
    msg_text_t msg{author, text, channel_name, dt};

    a_channel_ptr->notification(msg);
    auto history = a_channel_ptr->get_history();

    EXPECT_EQ(history.empty(), false);
    EXPECT_EQ(history.front(), msg);
};

TEST_F(TestChannel, testChannelSendFrontend) {
    EXPECT_EQ(id_result_func, 0);
    identifier_t a_client_id = 137;
    auto a_channel_ptr = std::make_shared<Channel>(config);
    Config::JoinChannelConfig join_config;
    join_config.client_id = a_client_id;
    join_config.send_front_callback =  [this](identifier_t client_id, const msg_text_t& msg) {
        this->fake_send_to_frontend(client_id, msg);
    };

    a_channel_ptr->join_channel(join_config);

    auto author = "author";
    auto text = "msg text";
    DateTime dt;
    msg_text_t msg{author, text, channel_name, dt};

    a_channel_ptr->notification(msg);
    EXPECT_EQ(id_result_func, 1);
};

TEST_F(TestChannel, testChannelSubs) {
    EXPECT_EQ(id_result_func, 0);
    const int result = 100;
    identifier_t a_client_id = 0;
    auto a_channel_ptr = std::make_shared<Channel>(config);

    Config::JoinChannelConfig join_config;
    join_config.send_front_callback =  [this](identifier_t client_id, const msg_text_t& msg) {
        this->fake_send_to_frontend(client_id, msg);
    };
    for(int i=0; i<result; ++i, ++a_client_id) {
        join_config.client_id = a_client_id;
        a_channel_ptr->join_channel(join_config);
    };
    auto subs = a_channel_ptr->get_subscribers();

    EXPECT_EQ(subs.size(), result);

    int id_sub = 0;
    for(const auto& sub : subs) {
         EXPECT_EQ(sub, id_sub);
         id_sub++;
    }
};

TEST_F(TestChannel, testChannelSendManyFrontend) {
    EXPECT_EQ(id_result_func, 0);
   
    const int result = 100;
    identifier_t a_client_id = 137;
    auto a_channel_ptr = std::make_shared<Channel>(config);
    EXPECT_EQ(a_channel_ptr->get_subscribers().size(), 0);

    Config::JoinChannelConfig join_config;
    join_config.send_front_callback =  [this](identifier_t client_id, const msg_text_t& msg) {
        this->fake_send_to_frontend(client_id, msg);
    };

    for(int i=0; i<result; ++i, ++a_client_id) {
        join_config.client_id = a_client_id;
        a_channel_ptr->join_channel(join_config);
    }
    EXPECT_EQ(a_channel_ptr->get_subscribers().size(), result);
    auto author = "author";
    auto text = "msg text";
    DateTime dt;
    msg_text_t msg{author, text, channel_name, dt};

    a_channel_ptr->notification(msg);
    EXPECT_EQ(id_result_func, result);
};

TEST_F(TestChannel, testChannelSendOneNotifyForMany) {
    EXPECT_EQ(id_result_func, 0);
   
    const int result = 100;
    identifier_t a_client_id = 137;
    auto a_channel_ptr = std::make_shared<Channel>(config);
    EXPECT_EQ(a_channel_ptr->get_subscribers().size(), 0);

    Config::JoinChannelConfig join_config;
    auto author = "author";
    auto text = "msg text";
    DateTime dt;
    msg_text_t msg{author, text, channel_name, dt};
    join_config.send_front_callback =  [this, msg](identifier_t a_client_id, const msg_text_t& a_msg) {
        EXPECT_EQ(msg, a_msg);
        this->fake_send_to_frontend(a_client_id, a_msg);
    };

    for(int i=0; i<result; ++i, ++a_client_id) {
        join_config.client_id = a_client_id;
        a_channel_ptr->join_channel(join_config);
    }
    EXPECT_EQ(a_channel_ptr->get_subscribers().size(), result);
    

    a_channel_ptr->notification(msg);
    EXPECT_EQ(id_result_func, result);
};