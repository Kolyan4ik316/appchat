#include "gtest/gtest.h"
#include "protocol/protocol.h"

TEST(TestProtocol, testRegHasField) {
    std::string login = "vasiliy";
    std::string passsword = "a1s22g_1";

    auto reg_request_bin = Protocol::MsgFactory::create_reg_request(login, passsword);

    EXPECT_EQ(reg_request_bin->has_register_request(), true);
    EXPECT_EQ(!reg_request_bin->has_input_request(), true);
    EXPECT_EQ(!reg_request_bin->has_text_request(), true);
    EXPECT_EQ(!reg_request_bin->has_join_room_request(), true);
}

TEST(TestProtocol, testRegHasLongLoginPsw) {
    std::string login = "vasiliysdasdasdasadsdadsad asdsad";
    std::string passsword = "a1s22g_1sadasdsadsadasdsad1231231242142140856856";

    auto reg_request_bin = Protocol::MsgFactory::create_reg_request(login, passsword);

    EXPECT_EQ(reg_request_bin->has_register_request(), true);
    EXPECT_EQ(!reg_request_bin->has_input_request(), true);
    EXPECT_EQ(!reg_request_bin->has_text_request(), true);
    EXPECT_EQ(!reg_request_bin->has_join_room_request(), true);
}

TEST(TestProtocol, testRegHasEmptyLoginPsw) {
    std::string login = "";
    std::string passsword = "";

    auto reg_request_bin = Protocol::MsgFactory::create_reg_request(login, passsword);

    EXPECT_EQ(reg_request_bin->has_register_request(), true);
    EXPECT_EQ(!reg_request_bin->has_input_request(), true);
    EXPECT_EQ(!reg_request_bin->has_text_request(), true);
    EXPECT_EQ(!reg_request_bin->has_join_room_request(), true);
}

TEST(TestProtocol, testRegHasCorrectField) {
    std::string login = "vasiliy";
    std::string passsword = "a1s22g_1";

    auto reg_request_bin = Protocol::MsgFactory::create_reg_request(login, passsword);

    EXPECT_EQ(reg_request_bin->register_request().login(), login);
    EXPECT_EQ(reg_request_bin->register_request().password(), passsword);
}

TEST(TestProtocol, testRegHasCorrectLongLoginPsw) {
    std::string login = "vasilsdasafmdskndlskn dsmfkkdsnfkndsnfsdlnfkldsnflknsdfkndslkfniy";
    std::string passsword = "a1s22g_133289408324877gds8f7gds7gsd7g987dsfg87sd89g78ds7g7sdg987sd8g7987dsg";

    auto reg_request_bin = Protocol::MsgFactory::create_reg_request(login, passsword);

    EXPECT_EQ(reg_request_bin->register_request().login(), login);
    EXPECT_EQ(reg_request_bin->register_request().password(), passsword);
}

//*********************************************************************************************************************
TEST(TestProtocol, testAutoHasLoginPsw) {
    std::string login = "vasiliy";
    std::string passsword = "a1s22g_1";

    auto in_request_bin = Protocol::MsgFactory::create_input_request(login, passsword);

    EXPECT_EQ(!in_request_bin->has_register_request(), true);
    EXPECT_EQ(in_request_bin->has_input_request(), true);
    EXPECT_EQ(!in_request_bin->has_text_request(), true);
    EXPECT_EQ(!in_request_bin->has_join_room_request(), true);
}

TEST(TestProtocol, testAutoHasLongLoginPsw) {
    std::string login = "vasiliysdasdasdasadsdadsad asdsad";
    std::string passsword = "a1s22g_1sadasdsadsadasdsad1231231242142140856856";

    auto in_request_bin = Protocol::MsgFactory::create_input_request(login, passsword);

    EXPECT_EQ(!in_request_bin->has_register_request(), true);
    EXPECT_EQ(in_request_bin->has_input_request(), true);
    EXPECT_EQ(!in_request_bin->has_text_request(), true);
    EXPECT_EQ(!in_request_bin->has_join_room_request(), true);
}

TEST(TestProtocol, testAutoHasEmptyLoginPsw) {
    std::string login = "";
    std::string passsword = "";

    auto in_request_bin = Protocol::MsgFactory::create_input_request(login, passsword);

    EXPECT_EQ(!in_request_bin->has_register_request(), true);
    EXPECT_EQ(in_request_bin->has_input_request(), true);
    EXPECT_EQ(!in_request_bin->has_text_request(), true);
    EXPECT_EQ(!in_request_bin->has_join_room_request(), true);
}

TEST(TestProtocol, testAutoHasCorrectLoginPsw) {
    std::string login = "vasiliy";
    std::string passsword = "a1s22g_1";

    auto in_request_bin = Protocol::MsgFactory::create_input_request(login, passsword);

    EXPECT_EQ(in_request_bin->input_request().login(), login);
    EXPECT_EQ(in_request_bin->input_request().password(), passsword);
}

TEST(TestProtocol, testAutoHasCorrectLongLoginPsw) {
    std::string login = "vasilsdasafmdskndlskn dsmfkkdsnfkndsnfsdlnfkldsnflknsdfkndslkfniy";
    std::string passsword = "a1s22g_133289408324877gds8f7gds7gsd7g987dsfg87sd89g78ds7g7sdg987sd8g7987dsg";

    auto in_request_bin = Protocol::MsgFactory::create_input_request(login, passsword);

    EXPECT_EQ(in_request_bin->input_request().login(), login);
    EXPECT_EQ(in_request_bin->input_request().password(), passsword);
}
