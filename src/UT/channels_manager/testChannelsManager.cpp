#include "gtest/gtest.h"

#include "backend/channel/channels_manager.h"
#include "backend/backend.h"
#include <filesystem>

namespace fs = std::filesystem;

class TestChannelManagerConfig : public ::testing::Test {
protected:
    Config::ChannelsManagerConfig m_config;

    static constexpr auto login = "test_login";
    static constexpr auto client_id = 117;
    static constexpr auto channel_name = "test_name_channel";
    static constexpr auto text = "test messages";

    std::atomic<std::size_t> id_result_func;
    void fake_send_to_frontend(identifier_t client_id, const msg_text_t& msg) {
        ++id_result_func;
    }
};

class TestChannelManager : public TestChannelManagerConfig {
public:
    void SetUp() {
        // m_config.send_front_callback = [this](identifier_t client_id, const msg_text_t& msg)->void {
        //     this->fake_send_to_frontend(client_id, msg);
        // }; 
        id_result_func = 0;
    };

    void TearDown() override {
    };
};

TEST_F(TestChannelManager, testChannelsManagerJoinOne) {
    auto manager = std::make_unique<ChannelsManager>(m_config);
    auto channel_opt = manager->get_channel(channel_name);
    
    EXPECT_EQ(channel_opt, std::nullopt);
    EXPECT_TRUE(manager->get_client_rooms(client_id).empty());
    EXPECT_TRUE(manager->get_login(client_id).empty());
    
    Config::JoinChannelManagerConfig a_config;
    a_config.client_id = client_id;
    a_config.login = login;
    a_config.channel_name = channel_name;
    a_config.send_front_callback = [this](identifier_t client_id, const msg_text_t& msg)->void {
            this->fake_send_to_frontend(client_id, msg);
    }; 
    manager->join(a_config);

    auto opt_channel = manager->get_channel(channel_name);
    EXPECT_TRUE(opt_channel != std::nullopt);
    if (opt_channel) {
        EXPECT_EQ(opt_channel.value()->get_subscribers(), std::deque<identifier_t> {client_id});
        EXPECT_TRUE(opt_channel.value()->get_history().empty());
    }
    const std::deque<std::string> rooms {channel_name};
    EXPECT_EQ(manager->get_client_rooms(client_id), rooms);
    EXPECT_EQ(manager->get_login(client_id), login);
};

TEST_F(TestChannelManager, testChannelsManagerSendOne) {
    auto manager = std::make_unique<ChannelsManager>(m_config);
    auto channel_opt = manager->get_channel(channel_name);
    
    EXPECT_EQ(channel_opt, std::nullopt);
    EXPECT_TRUE(manager->get_client_rooms(client_id).empty());
    EXPECT_TRUE(manager->get_login(client_id).empty());
    
    Config::JoinChannelManagerConfig a_config;
    a_config.client_id = client_id;
    a_config.login = login;
    a_config.channel_name = channel_name;
    a_config.send_front_callback = [this](identifier_t client_id, const msg_text_t& msg)->void {
            this->fake_send_to_frontend(client_id, msg);
    }; 
    manager->join(a_config);

    msg_text_t msg {login, text, channel_name, DateTime()};
    manager->send_to_channel(msg);

    auto opt_channel = manager->get_channel(channel_name);
    EXPECT_TRUE(opt_channel != std::nullopt);
    if (opt_channel.has_value()) {
        EXPECT_EQ(opt_channel.value()->get_subscribers(), std::deque<identifier_t> {client_id});
        EXPECT_TRUE(opt_channel.value()->get_history().size() == 1);
        EXPECT_EQ(opt_channel.value()->get_history().front(), msg);
    }
};

TEST_F(TestChannelManager, testChannelsManagerLeaveOne) {
    auto manager = std::make_unique<ChannelsManager>(m_config);
    auto channel_opt = manager->get_channel(channel_name);
    
    EXPECT_EQ(channel_opt, std::nullopt);
    EXPECT_TRUE(manager->get_client_rooms(client_id).empty());
    EXPECT_TRUE(manager->get_login(client_id).empty());
    
    Config::JoinChannelManagerConfig a_config;
    a_config.client_id = client_id;
    a_config.login = login;
    a_config.channel_name = channel_name;
    a_config.send_front_callback = [this](identifier_t client_id, const msg_text_t& msg)->void {
            this->fake_send_to_frontend(client_id, msg);
    }; 
    manager->join(a_config);
    
    manager->leave(client_id, channel_name);

    auto opt_channel = manager->get_channel(channel_name);
    EXPECT_TRUE(opt_channel != std::nullopt);
    if (opt_channel) {
        EXPECT_TRUE(opt_channel.value()->get_subscribers().empty());
        EXPECT_TRUE(opt_channel.value()->get_history().empty());
    }
    EXPECT_TRUE(manager->get_client_rooms(client_id).empty());
    EXPECT_TRUE(manager->get_login(client_id).empty());
};
