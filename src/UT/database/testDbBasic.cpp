#include "DbConfig.h"
#include <memory>
#include <filesystem>

namespace fs = std::filesystem;

class TestDbBasic : public DbConfig {
public:
    void SetUp() {
        db_config.FolderPath = std::string(fs::current_path()) + "/Temp/";
        db_config.ConnectionString = "file://" + db_config.FolderPath + "Test.db";
        m_db = std::make_shared<Storage::SqliteDatabase>(db_config);
        ASSERT_EQ(m_db->open(), true);

        m_db->create_table_history();
        m_db->create_table_logins();
    };

    void TearDown() override {
        m_db->close();
        fs::remove(db_config.FolderPath + "Test.db");
        fs::remove_all(db_config.FolderPath);
    };
};

TEST_F(TestDbBasic, testCreate) {
    EXPECT_EQ(m_db->open(), true);
};

TEST_F(TestDbBasic, testCheckLogin) {
    EXPECT_EQ(m_db->add_logins("AAA", 1, "123"), true); 
    EXPECT_EQ(m_db->get_loginid("AAA"), 1);

    EXPECT_EQ(m_db->check_client("AAA", "123"), 1); 
};

TEST_F(TestDbBasic, testLognLoginPsw) {
    auto login = "AAAAAAAAA12312341";
    auto password = "123asdsd12 das @# adf fsdff .    ";
    EXPECT_EQ(m_db->add_logins(login, 1, password), true); 
    EXPECT_EQ(m_db->check_client(login, password), 1); 
};

TEST_F(TestDbBasic, testCheckFakeLogin1) {
    EXPECT_EQ(m_db->check_client("AAA", "123"), -1); 
};

TEST_F(TestDbBasic, testCheckFakeLogin2) {
    EXPECT_EQ(m_db->add_logins("AAA", 1, "123"), true); 
    EXPECT_EQ(m_db->check_client("AAA", ""), -1); 
};

TEST_F(TestDbBasic, testCheckFakeLogin3) {
    EXPECT_EQ(m_db->add_logins("AAA", 1, "123"), true); 
    EXPECT_EQ(m_db->check_client("", "1"), -1); 
};

TEST_F(TestDbBasic, testEmptyHistory) {
    EXPECT_EQ(m_db->get_history("nochannel").empty(), true); 
};