#include "DbConfig.h"
#include <memory>
#include <filesystem>

namespace fs = std::filesystem;

class TestDbDateTime : public DbConfig {
public:
    void SetUp() {
        db_config.FolderPath = std::string(fs::current_path()) + "/Temp/";
        db_config.ConnectionString = "file://" + db_config.FolderPath + "Test.db";
        m_db = std::make_shared<Storage::SqliteDatabase>(db_config);
        ASSERT_EQ(m_db->open(), true);

        m_db->create_table_history();
        m_db->create_table_logins();
    };

    void TearDown() override {
        m_db->close();
        fs::remove(db_config.FolderPath + "Test.db");
        fs::remove_all(db_config.FolderPath);
    };
};

TEST_F(TestDbDateTime, testTime) {
    Time time = {0, 1, 1};
    DateTime dt;
    dt.time = time;

    msg_text_t msg {author, text, channel_name, dt};

    EXPECT_EQ(m_db->get_history(channel_name).empty(), true); 

    EXPECT_EQ(m_db->save_text_message(msg), true);
    std::deque<msg_text_t> history;
    history = m_db->get_history(channel_name);
    
    EXPECT_EQ(history.empty(), false);
    EXPECT_EQ(history.front(), msg);

    EXPECT_EQ(history.front().dt, dt);
};