#include "datetime.h"

#include <tuple>

bool operator== (const Time& lhs, const Time& rhs) {
    return lhs.hours == rhs.hours && lhs.minutes == rhs.minutes && lhs.seconds == rhs.seconds;
}

bool operator< (const Time& lhs, const Time& rhs) {
    return std::tuple(lhs.hours, lhs.minutes, lhs.seconds)
         < std::tuple(rhs.hours, rhs.minutes, rhs.seconds);
}

bool operator== (const Date& lhs, const Date& rhs) {
    return lhs.day == rhs.day && lhs.month == rhs.month && lhs.year == rhs.year;
}

bool operator< (const Date& lhs, const Date& rhs) {
    return std::tuple(lhs.year, lhs.month, lhs.day)
         < std::tuple(rhs.year, rhs.month, rhs.day);
}

bool operator== (const DateTime& lhs, const DateTime& rhs) {
    return lhs.time == rhs.time && lhs.date == rhs.date;
}

bool operator< (const DateTime& lhs, const DateTime& rhs) {
    return std::tuple(lhs.date, lhs.time)
         < std::tuple(rhs.date, rhs.time);
}
