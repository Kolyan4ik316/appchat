#ifndef MSGFACTORY_H
#define MSGFACTORY_H

#include "command_table.h"
#include "messages.pb.h"

#include <deque>

namespace Protocol {

using ptr_input_request_t   = std::unique_ptr<Serialize::InRequest>;
using ptr_reg_request_t     = std::unique_ptr<Serialize::RegRequest>;
using ptr_proto_request_t   = std::unique_ptr<Serialize::Request>;
using ptr_header_t          = std::unique_ptr<Serialize::Header>;
using ptr_proto_response_t  = std::unique_ptr<Serialize::Response>;
using ptr_datetime_t        = std::unique_ptr<Serialize::Datetime>;

constexpr std::size_t SIZE_HEADER = 2*(sizeof(uint32_t)+1) + (sizeof(uint64_t)+1);

class MsgFactory
{
public:
    MsgFactory() = default;

    [[nodiscard]]
    static ptr_proto_request_t create_input_request(const std::string& login, const std::string& password);

    [[nodiscard]]
    static ptr_proto_request_t create_reg_request(const std::string& login, const std::string& password);

    [[nodiscard]]
    static ptr_proto_request_t join_room_request(identifier_t, const std::string&);

    [[nodiscard]]
    static ptr_proto_request_t create_text_request(const msg_text_t&);

    [[nodiscard]]
    static ptr_proto_request_t create_history_request(
        identifier_t, const std::string& channel_name, DateTime since
    );

    [[nodiscard]]
    static ptr_header_t create_header(TypeCommand command, std::size_t length);

    [[nodiscard]]
    static ptr_proto_response_t create_input_response(int client_id);

    [[nodiscard]]
    static ptr_proto_response_t create_reg_response(int client_id);

    [[nodiscard]]
    static ptr_proto_response_t create_text_response(const msg_text_t &);

    [[nodiscard]]
    static ptr_proto_response_t create_join_room_response(const std::string&, bool flag);

    [[nodiscard]]
    static ptr_proto_response_t create_history_response(const std::string&, const std::deque<msg_text_t>&);

    [[nodiscard]]
    static std::vector<uint8_t> serialize_response(ptr_header_t&& header_ptr, ptr_proto_response_t&& response_ptr);

    [[nodiscard]]
    static std::vector<uint8_t> serialize_request(ptr_header_t&& header_ptr, ptr_proto_request_t&& request_ptr);

    [[nodiscard]]
    static std::vector<uint8_t> create_buffer_join_channel(identifier_t, const std::string&);

    static msg_text_t parse_text_req(Serialize::Request);
    static msg_text_t parse_text_res(Serialize::Response);
    static msg_text_t parse_text_res(const Serialize::TextResponse& );
private:
    static ptr_datetime_t create_datetime(const msg_text_t&);
};

} // end namespace

inline std::ostream& operator<<(std::ostream& stream, const Serialize::Header msg) {
    stream << "header size: "   << msg.ByteSizeLong() << " bytes" << std::endl;
    stream << "version:"        << msg.version() << std::endl;
    stream << "command:"        << msg.command() << std::endl;
//    stream << "time:"           << msg.time() << std::endl;
    stream << "length:"         << msg.length() << std::endl;

    return stream;
}

inline std::ostream& operator<<(std::ostream& stream, const Serialize::InRequest msg) {
    stream << "read InputRequest: " << msg.ByteSizeLong() << " bytes" << std::endl;
    stream << "login:"              << msg.login() << std::endl;
    stream << "password:"           << msg.password() << std::endl;

    return stream;
}

inline std::ostream& operator<<(std::ostream& stream, const Serialize::Request msg) {
    if (msg.has_input_request()) {
        stream << msg;
    }

    return stream;
}
#endif // MSGFACTORY_H
