#include "backend.h"

namespace ba = boost::asio;

Backend::Backend(Storage::database_ptr _db) :
    db(_db)
{
    Config::ChannelsManagerConfig config;
    config.db = db;
    m_channel_manager = std::make_unique<ChannelsManager>(config);
    BOOST_LOG_TRIVIAL(info) << "Backend create" ;

    m_tasks_queue = std::make_unique<Thread::Queue>();
    m_tasks_queue->start();    
}

void Backend::process_msg(connection_ptr a_fronend_ptr, Serialize::Header a_header, std::vector<uint8_t> a_buffer_msg) {
    Config::Package package;
    package.frontend_ptr = a_fronend_ptr;

    bool parse = package.request.ParseFromArray(a_buffer_msg.data(), static_cast<int>(a_buffer_msg.size()));
    if (parse) {
        BOOST_LOG_TRIVIAL(info) << "parse input_request: OK" ;
    } else {
        BOOST_LOG_TRIVIAL(error) << "parse input_request: FAIL" ;
    }

    switch (static_cast<TypeCommand>(a_header.command())) {
        case TypeCommand::AuthorisationRequest:
        {
            BOOST_LOG_TRIVIAL(info) << "add do_autorisation in queue" ;
            // std::lock_guard<std::mutex> lk(cv_m);
            // m_thread_queue.emplace(std::async(std::launch::deferred, 
            //                 &Backend::do_autorisation, this, package)
            // );
            m_tasks_queue->add_task(std::async(std::launch::deferred, 
                            &Backend::do_autorisation, this, package));
            break;
        }
        case TypeCommand::RegistrationRequest:
        {
            BOOST_LOG_TRIVIAL(info) << "add do_registration in queue" ;
            // std::lock_guard<std::mutex> lk(cv_m);
            // m_thread_queue.emplace(std::async(std::launch::deferred, 
            //                 &Backend::do_registration, this, package));
            m_tasks_queue->add_task(std::async(std::launch::deferred, 
                            &Backend::do_registration, this, package));
            break;
        }
        case TypeCommand::JoinRoomRequest:
        {
            BOOST_LOG_TRIVIAL(info) << "add do_join_room in queue";
            // std::lock_guard<std::mutex> lk(cv_m);
            // m_thread_queue.emplace(std::async(std::launch::deferred, 
            //                 &Backend::do_join_room, this, package));
            m_tasks_queue->add_task(std::async(std::launch::deferred, 
                            &Backend::do_join_room, this, package));
            break;
        }
        case TypeCommand::EchoRequest:
        {
            BOOST_LOG_TRIVIAL(info) << "add do_text_msg in queue" ;
            // std::lock_guard<std::mutex> lk(cv_m);
            // m_thread_queue.emplace(std::async(std::launch::deferred, 
            //                 &Backend::do_text_msg, this, package));
            m_tasks_queue->add_task(std::async(std::launch::deferred, 
                            &Backend::do_text_msg, this, package));
            break;
        }
        case TypeCommand::HistoryRequest:
        {
            // boost::asio::post(thread_pool->get_executor(), std::bind(&Connection::do_history,
            //                             this,
            //                             new_request));
            BOOST_LOG_TRIVIAL(info) << "add do_history in queue";
            m_tasks_queue->add_task(std::async(std::launch::deferred, 
                            &Backend::do_history, this, package));
            break;
        }
    default:
        BOOST_LOG_TRIVIAL(error) << "read Unknown request";
        break;
    }

    // cv.notify_one();
    // cv.notify_all();
    m_tasks_queue->notify();
}

void Backend::do_autorisation(Config::Package a_package) {
    BOOST_LOG_TRIVIAL(info) << "called do_autorisation(), thread_id=" << std::this_thread::get_id();
    if (a_package.request.has_input_request()) {
        BOOST_LOG_TRIVIAL(info) << "request include input_request" ;

        // @todo check db
        Config::UserInfo userInfo;
        userInfo.login = a_package.request.input_request().login();
        userInfo.password = a_package.request.input_request().password();
        userInfo.client_id = db->check_client(userInfo.login, userInfo.password);

        BOOST_LOG_TRIVIAL(info) << "read from inp_request: login=" << userInfo.login 
                                << ", pwd=" << userInfo.password 
                                << ", id=" << userInfo.client_id;

        // a_package.frontend_ptr->send_auto_response(userInfo.client_id);
        // m_thread_queue.emplace(std::async(std::launch::deferred, 
        //                     &IConnection::send_auto_response, a_package.frontend_ptr, userInfo.client_id));
        m_tasks_queue->add_task(std::async(std::launch::deferred, 
                            &IConnection::send_auto_response, a_package.frontend_ptr, userInfo.client_id));

        BOOST_LOG_TRIVIAL(info) << "Authorization: " << (userInfo.client_id!=-1 ? "OK" : "FAIL");
// @todo incorrect inout
        if (userInfo.client_id==-1) {
            a_package.frontend_ptr->free_connection();
        } else {
            m_client_connections.emplace(userInfo.client_id, a_package.frontend_ptr);
            m_clients_info.emplace(userInfo.client_id, userInfo);
            m_connection_client.emplace(a_package.frontend_ptr, userInfo.client_id);
        }
    }
    else {
        BOOST_LOG_TRIVIAL(error) << "new_request not have input_request";
        a_package.frontend_ptr->free_connection();
    }
}

void Backend::do_registration(Config::Package a_package) {
    if (a_package.request.has_register_request()) {
        
        Config::UserInfo userInfo;
        userInfo.login = a_package.request.register_request().login();
        userInfo.password = a_package.request.register_request().password();
        userInfo.client_id = db->get_loginid(userInfo.login);

        BOOST_LOG_TRIVIAL(info) << "read from reg_request: login=" << userInfo.login 
                                << ", pwd=" << userInfo.password;

        
        if (userInfo.client_id == -1) {
            userInfo.client_id = generate_client_id();
            // @todo add_logins(userInfo);
            db->add_logins(userInfo.login, userInfo.client_id, userInfo.password);
        }
        else {
            BOOST_LOG_TRIVIAL(warning) << "this client was add to db early";
            userInfo.client_id = -1;
        }

        // a_package.frontend_ptr->send_reg_response(userInfo.client_id);
        // m_thread_queue.emplace(std::async(std::launch::deferred, 
        //                     &IConnection::send_reg_response, a_package.frontend_ptr, userInfo.client_id));
        m_tasks_queue->add_task(std::async(std::launch::deferred, 
                            &IConnection::send_reg_response, a_package.frontend_ptr, userInfo.client_id));

        if (userInfo.client_id != -1) {
            BOOST_LOG_TRIVIAL(info) << "Registration successfully completed, id=" << userInfo.client_id;
            m_client_connections.emplace(userInfo.client_id, a_package.frontend_ptr);
            m_clients_info.emplace(userInfo.client_id, userInfo);
            m_connection_client.emplace(a_package.frontend_ptr, userInfo.client_id);
        }
        else {
            BOOST_LOG_TRIVIAL(error) << "Registration failed, id=" << userInfo.client_id;;
            // @todo incorrect inout
            // a_package.frontend_ptr->free_connection();
        }
    } else {
        BOOST_LOG_TRIVIAL(error) << "new_request not have reg_request";
        a_package.frontend_ptr->free_connection();
    }
}

void Backend::do_join_room(Config::Package a_package) {
    BOOST_LOG_TRIVIAL(info) << "called do_join_room(), thread_id=" << std::this_thread::get_id();
    if (a_package.request.has_join_room_request()) {
        auto self(shared_from_this());

        auto id = a_package.request.join_room_request().client_id();
        auto it_info = m_clients_info.find(id);
        if (it_info == m_clients_info.end()) {
            BOOST_LOG_TRIVIAL(error) << "Not found client_id=" << id << " in data";
            return;
        }
        it_info->second.m_channel_name = a_package.request.join_room_request().channel_name();
        BOOST_LOG_TRIVIAL(info) << "req to join channel: " << it_info->second.m_channel_name;

        bool flag = true;
        Config::JoinChannelManagerConfig config;
        config.client_id = it_info->second.client_id;
        config.login = it_info->second.login;
        config.channel_name = it_info->second.m_channel_name;
        config.send_front_callback = [this](identifier_t client_id, const msg_text_t& msg)->void {
            this->send_to_frontend(client_id, msg);
        };

        flag = m_channel_manager->join(config);

        auto v_channel_name = it_info->second.m_channel_name;

        // a_package.frontend_ptr->send_join_room_response(v_channel_name, flag);
        // m_thread_queue.emplace(std::async(std::launch::deferred, 
        //                     &IConnection::send_join_room_response, a_package.frontend_ptr, v_channel_name, flag));
        m_tasks_queue->add_task(std::async(std::launch::deferred, 
                            &IConnection::send_join_room_response, a_package.frontend_ptr, v_channel_name, flag));

        BOOST_LOG_TRIVIAL(info) << "send join_room_response";

    }else {
        BOOST_LOG_TRIVIAL(error) << "request without join_room_request";
    }
}

void Backend::do_text_msg(Config::Package a_package) {
    BOOST_LOG_TRIVIAL(info) << "called do_text_msg(), thread_id=" << std::this_thread::get_id();
    if (a_package.request.has_text_request()) {
        msg_text_t a_msg = Protocol::MsgFactory::parse_text_req(a_package.request);
        BOOST_LOG_TRIVIAL(info) << "login=" << a_msg.author << ", channel=" << a_msg.channel_name 
                                << ", text=" << a_msg.text
                                << ", " << a_msg.dt.time.hours << ":"
                                << a_msg.dt.time.minutes;

        m_channel_manager->send_to_channel(a_msg);
        db->save_text_message(a_msg);
    } else {
        BOOST_LOG_TRIVIAL(error) << "request without text_request";
    }
}

void Backend::do_history(Config::Package a_package) {
    BOOST_LOG_TRIVIAL(info) << "called do_history(), thread_id=" << std::this_thread::get_id();
    
    if (a_package.request.has_history_request()) {
        // auto self = shared_from_this();
        // const auto channel_name = a_package.request.history_request().channel_name();

        Config::HistoryChannelManagerConfig config;
        // config.client_id = it_info->second.client_id;
        // config.login = it_info->second.login;
        config.channel_name = a_package.request.history_request().channel_name();

        const auto since = a_package.request.history_request().since();
        config.since = DateTime(
            Time(since.seconds(), since.minutes(), since.hours()),
            Date(since.day(), since.month(), since.year())
        );

        auto client_id = a_package.request.history_request().client_id();
        config.send_history_callback = [this, client_id](const std::string& channel_name, 
                                                const std::deque<msg_text_t>& history)->void {
            send_history_frontend(client_id, channel_name, history);
        };

        m_channel_manager->send_history(config);
        // ChannelsManager::Instance().send_history(self, channel_name);
    } else {
        BOOST_LOG_TRIVIAL(error) << "request without history_request";
    }
}

void Backend::leave_from_channels(connection_ptr a_frontend_ptr) {
        auto it = m_connection_client.find(a_frontend_ptr);
        if (it != m_connection_client.end()) {
            const auto client_id = it->second;
            m_channel_manager->leave_from_all_channels(client_id);

            m_connection_client.erase(it);
            m_client_connections.erase(client_id);
            m_clients_info.erase(client_id);
        }
        
}
