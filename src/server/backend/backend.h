#ifndef BACKEND_H
#define BACKEND_H

#include "backend/ibackend.h"
#include "log/logger.h"
#include "storage/sqlitedatabase.h"
#include "channel/channels_manager.h"
#include "backend/ThreadQueue.h"

class Backend: public IBackend, public std::enable_shared_from_this<Backend>
{
public:
    Backend(Storage::database_ptr);

    void process_msg(connection_ptr, Serialize::Header header, std::vector<uint8_t> buffer_msg) override;

    void send_to_frontend(identifier_t a_client_id, const msg_text_t& a_msg) override {
        auto fptr = m_client_connections.find(a_client_id);
        if (fptr != m_client_connections.end()) {
            BOOST_LOG_TRIVIAL(info) << "Send to client_id=" << a_client_id;
            fptr->second->send_msg_to_client(a_msg);
        } else {
            BOOST_LOG_TRIVIAL(error) << "No found client to send msg";
        }
    }

    void send_history_frontend(identifier_t a_client_id, const std::string& channel_name, 
                                                const std::deque<msg_text_t>& history) override {
        auto fptr = m_client_connections.find(a_client_id);
        if (fptr != m_client_connections.end()) {
            BOOST_LOG_TRIVIAL(info) << "Send history to client_id=" << a_client_id;
            fptr->second->send_history_response(channel_name, history);
        } else {
            BOOST_LOG_TRIVIAL(error) << "No found client to send history";
        }
    }

    void leave_from_channels(connection_ptr a_frontend_ptr) override;

     Storage::database_ptr get_database() override {
         return db;
     }

      ~Backend() {
        // frontend_ptr->free_connection();
        BOOST_LOG_TRIVIAL(info) << "Backend destr" ;
    }
private:
    Storage::database_ptr db;
    channels_manager_ptr m_channel_manager;
    std::unique_ptr<Thread::Queue> m_tasks_queue;

    std::unordered_map<identifier_t, connection_ptr> m_client_connections;
    std::unordered_map<connection_ptr, identifier_t> m_connection_client;
    std::unordered_map<identifier_t, Config::UserInfo> m_clients_info;

    identifier_t generate_client_id() {
        return rand() % INT_MAX;
    }

    void do_autorisation(Config::Package);
    void do_registration(Config::Package);
    void do_join_room(Config::Package);
    void do_text_msg(Config::Package);
    void do_history(Config::Package);
};

#endif // BACKEND_H
