#ifndef CHANNEL_H
#define CHANNEL_H

#include <unordered_map>
#include <mutex>

#include "backend/channel/ichannel.h"
#include "log/logger.h"
#include "backend/ibackend.h"


class Channel : public IChannel
{
public:

    Channel(const Config::CreateChannelConfig&);

    void join_channel(const Config::JoinChannelConfig &) override;

    void leave(identifier_t) override;

    void notification(const msg_text_t &) override;

    std::string get_channel_name() const override { 
        return m_config.channel_name; 
    }

    std::deque<identifier_t> get_subscribers() const override;

    std::deque<msg_text_t> get_history() const override;

    /**
     * @todo client_id
     * */
    void send_history(const Config::HistoryChannelConfig &) override;

    ~Channel() {
        // lock?
        // for(auto client_id:m_mut_subs.m_subscribers) {
        //     leave(client_id);
        // }

        
        std::for_each(m_mut_subs.m_subscribers.begin(), m_mut_subs.m_subscribers.end(),
            [this](identifier_t client_id) {
                this->leave(client_id);
            });
    }
private:
    Config::CreateChannelConfig m_config;

    /**
     * @todo create new class-wrapper for sync subscibers and history
     * 
     * */
    struct {
        mutable std::mutex mutex_subs;
        std::deque<identifier_t> m_subscribers;
    } m_mut_subs;

    struct {
        mutable std::mutex mutex_history;
        std::deque<msg_text_t> history_room;
    } m_mut_history;

    struct {
        mutable std::mutex mutex_callback;
        std::unordered_map<identifier_t, 
                            std::function<void(identifier_t, const msg_text_t&)>> front_callback;
    } m_mut_callback;

};

#endif // CHANNEL_H
