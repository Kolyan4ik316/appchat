#include "backend/channel/channel.h"
#include <algorithm>

Channel::Channel(const Config::CreateChannelConfig& a_config): m_config(a_config) {
     if (m_config.db) {
        std::lock_guard<std::mutex> lg(m_mut_history.mutex_history);
        m_mut_history.history_room = m_config.db->get_history(m_config.channel_name);

        BOOST_LOG_TRIVIAL(info) << "Create channel: " << m_config.channel_name;
     } else {
        BOOST_LOG_TRIVIAL(info) << "Failed to load history. Database pointer is nullptr.";
     }
}

void Channel::join_channel(const Config::JoinChannelConfig &a_config) {
    BOOST_LOG_TRIVIAL(info) << "Channel::join, count subs=" << m_mut_subs.m_subscribers.size();

    m_mut_subs.mutex_subs.lock();
    m_mut_subs.m_subscribers.push_back(a_config.client_id);
    m_mut_subs.mutex_subs.unlock();

    std::lock_guard<std::mutex> lg(m_mut_callback.mutex_callback);
    m_mut_callback.front_callback.emplace(a_config.client_id, a_config.send_front_callback);

    BOOST_LOG_TRIVIAL(info) << "finish Channel::join, count subs=" << m_mut_subs.m_subscribers.size();
}

void Channel::leave(identifier_t a_client_id) {
    m_mut_subs.mutex_subs.lock();
    const auto sub = std::find(m_mut_subs.m_subscribers.begin(), m_mut_subs.m_subscribers.end(), a_client_id);
    if (sub != m_mut_subs.m_subscribers.end()) {
        m_mut_subs.m_subscribers.erase(sub);
    }
    m_mut_subs.mutex_subs.unlock();

    std::lock_guard<std::mutex> lg(m_mut_callback.mutex_callback);
    m_mut_callback.front_callback.erase(a_client_id);
}

void Channel::notification(const msg_text_t& a_msg) {
    m_mut_subs.mutex_subs.lock();
    for(auto client_id : m_mut_subs.m_subscribers) {
        std::lock_guard<std::mutex> lg(m_mut_callback.mutex_callback);
        auto func = m_mut_callback.front_callback.find(client_id);
        if (func!=m_mut_callback.front_callback.end()) {
            func->second(client_id, a_msg);
        }
    }
    m_mut_subs.mutex_subs.unlock();

    BOOST_LOG_TRIVIAL(info) << "Channel::notification finish" ;

    std::lock_guard<std::mutex> lg(m_mut_history.mutex_history);
    m_mut_history.history_room.emplace_back(a_msg);
}

 std::deque<identifier_t> Channel::get_subscribers() const {
    std::lock_guard<std::mutex> lg(m_mut_subs.mutex_subs);
    return m_mut_subs.m_subscribers;
}

std::deque<msg_text_t> Channel::get_history() const {
    std::lock_guard<std::mutex> lg(m_mut_history.mutex_history);
    return m_mut_history.history_room;
}

void Channel::send_history(const Config::HistoryChannelConfig & a_config) {
    std::lock_guard<std::mutex> lg(m_mut_history.mutex_history);
    std::deque<msg_text_t> result;

    std::copy_if(
        m_mut_history.history_room.begin(),
        m_mut_history.history_room.end(),
        std::back_inserter(result),
        [since = a_config.since](const auto& msg) {
            return since < msg.dt;
        }
    );

    a_config.send_history_callback(m_config.channel_name, result);
}
