#ifndef CHANNELSMANAGER_H
#define CHANNELSMANAGER_H

#include <optional>
#include "channel.h"
#include "log/logger.h"


class ChannelsManager {
public:
    ChannelsManager(const Config::ChannelsManagerConfig&);

    bool join(const Config::JoinChannelManagerConfig &);

    void send_to_channel(const msg_text_t & data);

    void leave(identifier_t client_id, const std::string&);

    void leave_from_all_channels(identifier_t a_client_id);

    std::optional<channel_ptr_t> get_channel(const std::string& channel_name);

    std::deque<std::string> get_client_rooms(identifier_t) const;

    std::string get_login(identifier_t) const;

    /**
     * @brief Send room history to client
     *
     * @param subscriber client who needs to send message history
     * @param channel_name the name of the room from which the message history is requested
     * @todo working with client_id
     */
    void send_history(const Config::HistoryChannelManagerConfig &);

private:
    Config::ChannelsManagerConfig m_config;

    std::unordered_map<std::string, channel_ptr_t> channels;
    std::unordered_map<identifier_t, std::deque<std::string>> client_in_rooms;
    std::unordered_map<identifier_t, std::string> clientid_to_login;
};

using channels_manager_ptr = std::unique_ptr<ChannelsManager>;

#endif // CHANNELSMANAGER_H
