#ifndef SERVICECONFIG_H
#define SERVICECONFIG_H

#include "frontend/iconnection.h"
#include "protocol/protocol.h"
#include "storage/sqlitedatabase.h"

#include <functional>

using notify_callback_t = std::function<void(identifier_t, const msg_text_t&)>;
using history_callback_t = std::function<void(const std::string&, const std::deque<msg_text_t>&)>;

namespace Config {

struct UserInfo {
    identifier_t client_id;
    std::string m_channel_name;
    std::string login;
    std::string password;
};

struct Package {
    connection_ptr frontend_ptr;
    Serialize::Request request;
};

struct CreateChannelConfig {
    std::string channel_name;
    Storage::database_ptr db;
};

struct JoinChannelConfig {
    identifier_t client_id;
    notify_callback_t  send_front_callback;
};

struct HistoryChannelConfig {
    DateTime since;
    history_callback_t  send_history_callback;
};

struct ChannelsManagerConfig {
    Storage::database_ptr db;
};

struct JoinChannelManagerConfig {
    identifier_t client_id;
    std::string login;
    std::string channel_name;
    notify_callback_t  send_front_callback;
};

struct HistoryChannelManagerConfig {
    // identifier_t client_id;
    // std::string login;
    std::string channel_name;
    DateTime since;
    history_callback_t  send_history_callback;
};

}
#endif // SERVICECONFIG_H
