#ifndef IBACKEND_H
#define IBACKEND_H

#include "backend/ServiceConfig.h"
#include "storage/database.h"

class IBackend
{
public:
    virtual void process_msg(connection_ptr, Serialize::Header, std::vector<uint8_t> buffer_msg) = 0;

    virtual Storage::database_ptr get_database() = 0;

    virtual void send_to_frontend(identifier_t, const msg_text_t&) = 0;

    virtual void send_history_frontend(identifier_t a_client_id, const std::string& channel_name, 
                                                const std::deque<msg_text_t>& history) = 0;

    virtual void leave_from_channels(connection_ptr) = 0;

    virtual ~IBackend() = default;
};

using backend_ptr = std::shared_ptr<IBackend>;

#endif // IBACKEND_H
