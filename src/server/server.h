#ifndef SERVER_H
#define SERVER_H

#include <boost/asio.hpp>
#include <boost/asio/thread_pool.hpp>
#include <memory>

#include "frontend/frontend.h"
#include "backend/backend.h"

/**
 * @brief Async TCP Server.
 * @details Async TCP Server handling incoming tcp conntection on port SERVER_DEFAULT_PORT.
 * @todo transform to Load Balancer
 */
class Server {
public:

    Server(backend_ptr a_backend_server, unsigned short port):
        m_backend_server(a_backend_server),
        endpoint(boost::asio::ip::tcp::v4(), port),
        acceptor(io_service, endpoint)
    {
        scan_acception();
    }

    /**
     * @brief Run server.
     */
    void run() {
        boost::asio::signal_set sig(io_service, SIGINT);
        sig.async_wait(std::bind(&Server::signal_handler,
                       this,
                       std::placeholders::_1,
                       std::placeholders::_2));

        io_service.run();
    }

private:
    backend_ptr m_backend_server;

    boost::asio::io_service io_service;
    boost::asio::ip::tcp::endpoint endpoint;
    boost::asio::ip::tcp::acceptor acceptor;
    
    std::vector<connection_ptr> pool_connections;

    void scan_acception() {
        acceptor.async_accept([this](const boost::system::error_code& error, boost::asio::ip::tcp::socket a_socket) {
            if (!error) {               
                auto front = std::make_shared<Frontend>(m_backend_server, std::move(a_socket));
                pool_connections.push_back(front);
                front->start();
            }
            scan_acception();
        });
    }

    void signal_handler(const boost::system::error_code&, int) {
        BOOST_LOG_TRIVIAL(fatal) << "called signal_handler()";
        for(auto connection:pool_connections) {
            connection->free_connection();
        }
        io_service.stop();
        acceptor.close();
        BOOST_LOG_TRIVIAL(fatal) << "stop server done";
    }

};

#endif // SERVER_H






