#include "frontend/frontend.h"

namespace ba = boost::asio;

// ************************************* replace Frontend? ******************************************************************
void Frontend::async_read_pb_header() {
    BOOST_LOG_TRIVIAL(info) << "called async_read_pb_header(), thread_id=" << std::this_thread::get_id();
    ba::async_read(socket, ba::buffer(buffer_header),
                                          std::bind(&Frontend::do_parse_pb_header,
                                                    shared_from_this(),
                                                    std::placeholders::_1,
                                                    std::placeholders::_2));
}

void Frontend::do_parse_pb_header(boost::system::error_code error, std::size_t) {
    if (!error) {
        Serialize::Header new_header;
        bool flag = new_header.ParseFromArray(buffer_header.data(), Protocol::SIZE_HEADER);
        if (flag) {
            buffer_msg.resize(new_header.length());
            ba::async_read(socket,ba::buffer(buffer_msg.data(), new_header.length()),
                            [this, new_header](boost::system::error_code, std::size_t) {
                                auto self(shared_from_this());
                                backend->process_msg(self, new_header, buffer_msg);
                                async_read_pb_header();
                            });
        } else {
            BOOST_LOG_TRIVIAL(error) << "parse Header: FAIL";
            async_read_pb_header();
        }
    } else {
        BOOST_LOG_TRIVIAL(error) << "error read in async_read_pb_header():  " << error.value();
        free_connection();
    }
}

// ***********************************************************************************************************************************
void Frontend::send_msg_to_client(const msg_text_t & a_msg) {
    auto response_ptr = Protocol::MsgFactory::create_text_response(a_msg);
    auto header_ptr = Protocol::MsgFactory::create_header(TypeCommand::EchoResponse, response_ptr->ByteSizeLong());
    auto response_to_send = Protocol::MsgFactory::serialize_response(std::move(header_ptr), std::move(response_ptr));

    add_msg_send_queue(std::move(response_to_send));
}

void  Frontend::add_msg_send_queue(std::vector<uint8_t>&& bin_buffer) {
    bool process_write = !send_msgs_queue.empty();
    send_msgs_queue.push(std::move(bin_buffer));
    if (!process_write) {
        sending_msgs_to_client();
    }
}

void Frontend::sending_msgs_to_client() {
    ba::async_write(socket, ba::buffer(send_msgs_queue.front()),
        [this](boost::system::error_code ec, std::size_t) {
        if (!ec) {
            send_msgs_queue.pop();
            if (!send_msgs_queue.empty()) {
                sending_msgs_to_client();
            }
        }
        else {
            BOOST_LOG_TRIVIAL(error) << "error start_send_bin_buffers()";
            free_connection();
        }
    });
}

// ***********************************************************************************************************************************
// void Frontend::reuse(boost::asio::ip::tcp::socket&& _socket) {
//     if (busy) {
//         BOOST_LOG_TRIVIAL(fatal) << "reuse not free connection";
//         free_connection();
//     }
//     socket = std::move(_socket);
//     busy = true;
//     BOOST_LOG_TRIVIAL(info) << "reuse connection from " << socket.remote_endpoint().address().to_string()
//                             << ":" << socket.remote_endpoint().port();
// }

void Frontend::free_connection() {
    BOOST_LOG_TRIVIAL(warning) << "free_connection()";
    if (is_work) {
        BOOST_LOG_TRIVIAL(warning) << "need free_connection()";

        mtx_sock.lock();
        if(socket.is_open()) {
            boost::system::error_code ec;
            socket.shutdown(ba::ip::tcp::socket::shutdown_send, ec);
            if (ec) {
                BOOST_LOG_TRIVIAL(error) << "Error when shutdown socket.";
            }
            socket.close(ec);
            if (ec) {
                BOOST_LOG_TRIVIAL(error) << "Error when close socket.";
            }
        }
        mtx_sock.unlock();

        BOOST_LOG_TRIVIAL(warning) << "Close socket(frontend)";
        auto self(shared_from_this()); 
        backend->leave_from_channels(self);
        BOOST_LOG_TRIVIAL(warning) << "User exit. Leave from channels. Close socket. Connection saved. ";
        is_work = false;
    }
}

void Frontend::send_auto_response(identifier_t a_client_id) {
    BOOST_LOG_TRIVIAL(info) << "called send_autorisation_response(), thread_id=" << std::this_thread::get_id();
    auto response_ptr = Protocol::MsgFactory::create_input_response(a_client_id);
    auto header_ptr = Protocol::MsgFactory::create_header(TypeCommand::AutorisationResponse, response_ptr->ByteSizeLong());
    auto buffer_serialize = Protocol::MsgFactory::serialize_response(std::move(header_ptr), std::move(response_ptr));

    ba::async_write(socket, ba::buffer(buffer_serialize),
        [this](boost::system::error_code ec, std::size_t) {
        if (!ec) {
                BOOST_LOG_TRIVIAL(info) << "Autorisation response send: OK";
        }
        else {
            BOOST_LOG_TRIVIAL(error) << "Autorisation response send: FAIL";
            free_connection();
        }
    });
}

void Frontend::send_reg_response(identifier_t a_client_id) {
    BOOST_LOG_TRIVIAL(info) << "called send_reg_response(), thread_id=" << std::this_thread::get_id();
    auto response_ptr = Protocol::MsgFactory::create_reg_response(a_client_id);
    auto header_ptr = Protocol::MsgFactory::create_header(TypeCommand::RegistrationResponse, response_ptr->ByteSizeLong());
    auto buffer_serialize = Protocol::MsgFactory::serialize_response(std::move(header_ptr), std::move(response_ptr));

    boost::asio::async_write(socket, boost::asio::buffer(buffer_serialize),
        [this](boost::system::error_code ec, std::size_t) {
        if (!ec) {
                BOOST_LOG_TRIVIAL(info) << "Registration response send: OK";
        }
        else {
            BOOST_LOG_TRIVIAL(error) << "Registration response send: FAIL";
            free_connection();
        }
    });
}

void Frontend::send_join_room_response(std::string a_channel_name, bool flag) {
    BOOST_LOG_TRIVIAL(info) << "called send_join_room_response(), thread_id=" << std::this_thread::get_id();
    auto response_ptr = Protocol::MsgFactory::create_join_room_response(a_channel_name, flag);
    auto header_ptr = Protocol::MsgFactory::create_header(TypeCommand::JoinRoomResponse, response_ptr->ByteSizeLong());
    auto buffer_serialize = Protocol::MsgFactory::serialize_response(std::move(header_ptr), std::move(response_ptr));

//    add_msg_send_queue(std::move(buffer_serialize));
    ba::async_write(socket, ba::buffer(buffer_serialize),
        [this](boost::system::error_code ec, std::size_t) {
        if (!ec) {
                BOOST_LOG_TRIVIAL(info) << "Join_room response send: OK";
        }
        else {
            BOOST_LOG_TRIVIAL(error) << "Join_room response send: FAIL";
            free_connection();
        }
    });
}

void Frontend::send_history_response(
    const std::string& channel_name, const std::deque<msg_text_t>& history
) {
    BOOST_LOG_TRIVIAL(info) << "called send_history_response(), thread_id=" << std::this_thread::get_id();

    auto response_ptr = Protocol::MsgFactory::create_history_response(channel_name, history);
    auto header_ptr = Protocol::MsgFactory::create_header(
        TypeCommand::HistoryResponse, response_ptr->ByteSizeLong()
    );
    auto buffer_serialize = Protocol::MsgFactory::serialize_response(
        std::move(header_ptr), std::move(response_ptr)
    );    
    
    ba::async_write(socket, ba::buffer(buffer_serialize),
        [this](boost::system::error_code ec, std::size_t) {
        if (!ec) {
            BOOST_LOG_TRIVIAL(info) << "History response send: OK";
        }
        else {
            BOOST_LOG_TRIVIAL(error) << "History response send: FAIL";
            free_connection();
        }
    });
}
