#ifndef ICONNECTION_H
#define ICONNECTION_H

#include <iostream>
#include <memory>
#include <string>
#include <boost/asio.hpp>
#include "protocol/protocol.h"

/**
 * @brief Channel Connection Interface
 */
class IConnection
{
public:

    virtual void start() = 0;

    virtual void async_read_pb_header() = 0;
    virtual void do_parse_pb_header(boost::system::error_code, std::size_t) = 0;

    virtual void send_msg_to_client(const msg_text_t &) = 0;
    virtual void add_msg_send_queue(std::vector<uint8_t>&&) = 0;
    virtual void sending_msgs_to_client() = 0;

    virtual void send_auto_response(identifier_t) = 0;
    virtual void send_reg_response(identifier_t) = 0;
    virtual void send_join_room_response(std::string, bool) = 0;

    virtual void free_connection() = 0;

    virtual void send_history_response(const std::string&, const std::deque<msg_text_t>&) = 0;
    
    virtual ~IConnection() = default;
};

using connection_ptr = std::shared_ptr<IConnection>;

#endif // ICONNECTION_H
