#ifndef CONNECTION_H
#define CONNECTION_H

#include <memory>
#include <queue>
#include <mutex>
#include <vector>
#include <cassert>

#include "frontend/iconnection.h"
#include <atomic>
// need?
#include "backend/channel/channels_manager.h"
#include "log/logger.h"

/**
 * @brief Frontend class
 * @details It serves connected tcp client
 */
class Frontend : public IConnection, public std::enable_shared_from_this<Frontend>
{
public:

    explicit Frontend(backend_ptr ptr, 
                    boost::asio::ip::tcp::socket&& _socket):
        backend(ptr),
        socket(std::move(_socket)),
        is_work(true)
    {
        BOOST_LOG_TRIVIAL(info) << "new connection from " << socket.remote_endpoint().address().to_string()
                                << ":" << socket.remote_endpoint().port();
    }

    virtual void start() override {
        BOOST_LOG_TRIVIAL(info) << "Connection start read_request_header().";
        async_read_pb_header();
   }

    void send_msg_to_client(const msg_text_t &) override;
    void add_msg_send_queue(std::vector<uint8_t>&&) override;

    void send_auto_response(identifier_t a_client_id) override;
    void send_reg_response(identifier_t a_client_id) override;
    void send_join_room_response(std::string a_channel_name, bool flag) override;
    void send_history_response(const std::string&, const std::deque<msg_text_t>&) override;

    virtual void free_connection() override;

    ~Frontend() {
        BOOST_LOG_TRIVIAL(warning) << "desrt(frontend)";
        free_connection();
    }

private:
    backend_ptr backend;
    boost::asio::ip::tcp::socket socket;
    std::mutex mtx_sock;

    std::array<uint8_t, Protocol::SIZE_HEADER> buffer_header;
    std::vector<uint8_t> buffer_msg;

    std::queue<std::vector<uint8_t>> send_msgs_queue;

    std::atomic_bool is_work;

private:

    void async_read_pb_header() override;
    void do_parse_pb_header(boost::system::error_code, std::size_t) override;
    void sending_msgs_to_client() override;

    /**
     * ref
     * */
    
};

using frontend_ptr = std::shared_ptr<Frontend>;

#endif // CONNECTION_H
